import React from "react";
import LighthouseWorkInfo from "../WorkInfo/LighthouseWorkInfo.json";
export interface ICompanyCardProps {
	titleSection: {
		company: string,
		location: string,
		dates: string,
		jobTitle: string
	}
}

export interface ICompanyCardState {
	flipped: boolean
}

export class CompanyCard extends React.Component<ICompanyCardProps, ICompanyCardState> {

	constructor(props: ICompanyCardProps)
	{
		super(props);
		this.state = { flipped: false }
	}

	public render(): JSX.Element {
		const contents = this.state.flipped ? this.backCard() : this.frontCard();

		return (
			<div className="company-card-container" onClick={this.flipCard}>
				{contents}
			</div>)
	}


	private flipCard = () => this.setState({ flipped: !this.state.flipped });

	private frontCard(): JSX.Element
	{
		return (
			<div className="company-card">
				<div className="company-card-image-section">
					<img src="../../img/lighthouse_logo.png"></img>
				</div>
				<div className="company-card-title-section">
					<div className="company-card-title-section-company">
						<span>{this.props.titleSection.company}</span>
					</div>
					<div className="company-card-title-section-job-title">
						<span>{this.props.titleSection.jobTitle}</span>
					</div>
					<div className="company-card-title-section-location">
						<span>{this.props.titleSection.location}</span>
					</div>
					<div className="company-card-title-section-dates">
						<span>{this.props.titleSection.dates}</span>
					</div>
				</div>

			</div>
		)
	}

	private backCard(): JSX.Element
	{
		const textSections = LighthouseWorkInfo.text.map((text) => <li className='work-info-card-text-section'>{text}</li>)

		return (
			<div className="work-info-card">
				{textSections}
			</div>)
	}
}