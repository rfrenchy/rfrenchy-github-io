import React from "react";

import { ProfileImage } from "../ProfileImage/ProfileImage";
import { ProfileTitle } from "../ProfileTitle/ProfileTitle";

import { NavIconNames } from "../../Menu/NavButton/NavButton";

export class CardContainer extends React.Component {
	public render(): JSX.Element {

		const animationClass = "animated zoomIn fast";

		return (
			<div className={"start-container"} >
				<div className={`start-container-image-section ${animationClass}`}>{<ProfileImage />}</div>
				<div className={`start-container-text-section ${animationClass}`} >
					{< ProfileTitle title={"Ryan French"} description={"Software Engineer"} />}
				</div>
			</div >
		);
	}
}
