import React from "react";

import { IoIosPaper } from "react-icons/io";

export class CV extends React.Component {

	public render(): JSX.Element {
		return (
			<div className="cv-container">
				<div className="cv-wrapper">
					{/* <div className="cv-title"><span>CV</span></div> */}
					<div className="cv-title-wrapper">
						<div className="cv-icon"><IoIosPaper size={"120px"} /></div>
						<div className="cv-title">CV</div>
					</div>
					<div className="cv-download-doc-wrapper">
						<button className="cv-download-doc-button">.doc</button>
					</div>
					<div className="cv-download-pdf-wrapper">
						<button className="cv-download-pdf-button">.pdf</button>
					</div>
				</div>
			</div>)
	}
}
