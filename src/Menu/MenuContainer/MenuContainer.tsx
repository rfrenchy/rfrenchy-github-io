import React from "react";

import { NavButton, NavIconNames } from "../NavButton/NavButton";
import { MENU_BUTTON_DATA } from "./MenuData";

export interface IButtonModel {
	icon: NavIconNames,
	text: string
	delay: number;
}

export class MenuContainer extends React.Component {

	private navButtons: JSX.Element[];

	public constructor(props: any) {
		super(props);

		this.navButtons = MENU_BUTTON_DATA.map((button) =>
			<NavButton
				icon={button.icon}
				text={button.text}
				onClick={() => this.scrollToContainer(button.text)}
				zoomDelay={button.delay}
			/>,
		)
	}

	public render(): JSX.Element {

		const menuContainer = (
			<div className={"menu-container"}>
				{this.navButtons}
			</div>
		);

		return menuContainer;
	}

	public scrollToContainer(containerName: string): void {

		const className = `.${containerName}-container`
			.replace(" ", "-")
			.toLowerCase();

		const container = document.querySelector(className) as HTMLElement;

		if (container) {
			container.scrollIntoView({ behavior: "smooth", block: "center" });
		}
	}
}
