import { NavIconNames } from "../NavButton/NavButton";

interface IButtonModel {
	icon: NavIconNames;
	text: string;
	delay: number;
}

export const MENU_BUTTON_DATA: IButtonModel[] = [
	{
		icon: "work",
		text: "Work History",
		delay: 800,
	},
	{
		icon: "education",
		text: "Education",
		delay: 600,
	},
	{
		icon: "project",
		text: "Projects",
		delay: 600,
	},
	{
		icon: "contact",
		text: "Contact",
		delay: 800,
	}
]