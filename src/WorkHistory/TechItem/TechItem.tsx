
import React from "react";

interface ITechItemProps {
	name: string
}

export class TechItem extends React.Component<ITechItemProps> {

	public render(): JSX.Element {
		return <div className="tech-item">{this.props.name}</div>;
	}
}