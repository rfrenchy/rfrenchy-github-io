import React from "react";

import { ContactButton, ContactButtonIcons } from "./ContactButton";
import { ContactButtonData } from "./ContactData";
import { IoIosCall } from "react-icons/io";

export class ContactContainer extends React.Component {
	public render(): JSX.Element {

		const contactButtons = Object
			.values(ContactButtonData)
			.map((data) => <ContactButton icon={data.icon as ContactButtonIcons} link={data.link} />)

		return (
			<div className="contact-container">
				<div className="contact-title-container">
					<IoIosCall className={"nav-button-icon"} size={"10rem"} color={"white"} />
					<div className="contact-title">Contact</div>
				</div>
				<div className="contact-button-container">
					{contactButtons}
				</div>
			</div >)
	}
}

