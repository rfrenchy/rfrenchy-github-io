
import React from "react";

import { IoLogoOctocat } from "react-icons/io";
export class Projects extends React.Component {
	public render(): JSX.Element {
		return (
			<div className="projects-container">
				<IoLogoOctocat size={"15rem"} className={"projects-icon"} color={"#F5F5F5"} />
				<div className={"projects-title-wrapper"}><span>Projects</span></div>
				<div className={"projects-description-wrapper"}><span>(In Progress)</span></div>
			</div>)
	}
}