
import React from "react";

import { FaEnvelope, FaGithubSquare, FaLinkedin } from "react-icons/fa";

export type ContactButtonIcons = "EMAIL" | "GIT" | "LINKEDIN";

export interface IContactButtonProps {
	icon: ContactButtonIcons;
	link: string;
}

const ICON_SIZE = "2rem";

export class ContactButton extends React.Component<IContactButtonProps> {

	public render(): JSX.Element {
		return (
			<a href={this.props.link}>
				<div className="contact-button-wrapper animated zoomIn fast">
					{this.createIcon(this.props.icon)}
				</div>
			</a>)
	}

	private createIcon(image: ContactButtonIcons) {
		switch (image.toUpperCase()) {
			case "EMAIL": return <FaEnvelope className="contact-button-image" size={ICON_SIZE} color={"#2196F3"} />
			case "GIT": return <FaGithubSquare className="contact-button-image" size={ICON_SIZE} color={"#2196F3"} />
			case "LINKEDIN": return <FaLinkedin className="contact-button-image" size={ICON_SIZE} color={"#2196F3"} />
		}
	}
}