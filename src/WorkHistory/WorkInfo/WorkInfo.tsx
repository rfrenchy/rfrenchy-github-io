import React from "react";

import LighthouseWorkInfo from "./LighthouseWorkInfo.json";

export class WorkInfo extends React.Component {
	public render(): JSX.Element {

		const textSections = LighthouseWorkInfo.text.map((text) => <li className='work-info-card-text-section'>{text}</li>)

		return (
			<div className="work-info-card">
				{textSections}
			</div>)
	}
}