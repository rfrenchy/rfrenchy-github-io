
import React from "react";
import Swiper from "swiper";

import { CompanyCard, ICompanyCardProps } from "../CompanyCard/CompanyCard";
import { TechItemContainer } from "../TechItemContainer/TechItemContainer";
import { WorkInfo } from "../WorkInfo/WorkInfo";
import LighthouseData from "./Lighthouse.json";

const lighthouseData: ICompanyCardProps = {
	titleSection: {
		company: "Lighthouse Systems",
		location: "Crawley - UK",
		dates: "July 2017 - Present",
		jobTitle: "Software Developer"
	}
}

export class WorkHistory extends React.Component {
	public render(): JSX.Element {

		return (
			<div className="work-history-container">
				<div className="swiper-container">
					<div className="swiper-wrapper">
						<div className="swiper-slide">
							<div className="lighthouse-container">
							{[
								<CompanyCard titleSection={LighthouseData} />,
								// <WorkInfo/>,
								<TechItemContainer/>
							]}
							</div>
							<div className="work-history-background"></div>
						</div>
						<div className="swiper-slide">AMS</div>
						<div className="swiper-slide">Webactive</div>
					</div>
					<div className="swiper-pagination"></div>
				</div>
			</div>);
	}


	public componentDidMount(): void {
		new Swiper(".swiper-container", {
			pagination: {
				el: '.swiper-pagination',
				clickable: true
			},
		});
	}
}