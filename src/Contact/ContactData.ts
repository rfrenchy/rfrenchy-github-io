import { IContactButtonProps } from "./ContactButton";


export const ContactButtonData = {
	email: {
		title: "Email",
		icon: "EMAIL",
		link: "mailto:ryanfrench@outlook.com"
	},
	git: {
		title: "Github",
		icon: "GIT",
		link: "https://github.com/rfrenchy"
	},
	linkedin: {
		title: "LinkedIn",
		icon: "LINKEDIN",
		link: "https://www.linkedin.com/in/ryan-french-2a8449110/"
	},
}