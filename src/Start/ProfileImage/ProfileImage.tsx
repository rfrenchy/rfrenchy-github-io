import React from "react";

export class ProfileImage extends React.Component {

	public render() {

		const profileImage = (
			<div className="profile-image-container">
				<img className="profile-image" src={"../img/github_profile.png"} />
			</div>
		);

		return profileImage;
	}
}
