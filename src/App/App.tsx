
import React from "react";

import { CardContainer } from "../Start/CardContainer/CardContainer";
import { ContactContainer } from "../Contact/Contact";
import { MenuContainer } from "../Menu/MenuContainer/MenuContainer";
import { WorkHistory } from "../WorkHistory/WorkHistory/WorkHistory";
import { Education } from "../Education/Education";
import { Projects } from "../Projects/Projects";
import { ScrollButton } from "../ScrollButton/ScrollButton";

export class App extends React.Component {

	public render(): JSX.Element {
		return (
			<div className="app">
				<CardContainer />
				<MenuContainer />
				<WorkHistory />
				<Education />
				<Projects />
				<ContactContainer />
				<ScrollButton />
			</div>);
	}
}