
import React from "react";
import Swiper from "swiper";

import TechList from "./LighthouseTechList.json";
import { TechItem } from "../TechItem/TechItem";

export class TechItemContainer extends React.Component {

	public render(): JSX.Element {

		const techItems = TechList.items.sort()
			.map((item) => <TechItem name={item}/>)

		return (
			<div className="tech-wrapper">
				<div className="tech-item-container">
					{techItems}
				</div>
			</div>
		);
	}
}