
import React from "react";

interface IProfileTitleProps {
	title: string;
	description: string;
}

export class ProfileTitle extends React.Component<IProfileTitleProps> {

	public render() {
		const profileTitle = (
			<div className="main-container">
				<span className="main-title">{this.props.title}</span>
				<span className="main-description">{this.props.description}</span>
			</div >
		);

		return profileTitle;
	}
}
