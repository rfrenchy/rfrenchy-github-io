import React from "react";

import { IoMdBriefcase, IoIosCall, IoMdSchool, IoLogoOctocat, IoIosPaper } from "react-icons/io";

export type NavIconNames = "contact" | "education" | "work" | "project" | "cv";

interface IProps {
	text: string,
	icon: NavIconNames;
	onClick: () => void;
	/** Number of milliseconds to delay to zoom into view. */
	zoomDelay?: number;
}

export class NavButton extends React.Component<IProps> {

	private navIcon: JSX.Element;

	public constructor(props: IProps) {
		super(props);

		this.navIcon = this.setNavIcon(this.props.icon);

		if (props.zoomDelay === undefined) {
			props.zoomDelay = 0;
		}

		props.onClick = props.onClick.bind(this);
	}

	public render() {
		return (
			<button
				className={`nav-button animated zoomIn faster nav-button-delay-${this.props.zoomDelay}`}
				onClick={this.props.onClick}
			>
				<div className={"nav-button-wrapper"}>
					{this.navIcon}
					<span className={"nav-button-text"}>{this.props.text}</span>
				</div>
			</ button>
		);
	}

	private setNavIcon(iconName: NavIconNames): JSX.Element {
		switch (iconName) {
			case "education": return <IoMdSchool className={"nav-button-icon"} size={"2rem"} />
			case "contact": return <IoIosCall className={"nav-button-icon"} size={"2rem"} />;
			case "cv": return <IoIosPaper className={"nav-button-icon"} size={"2rem"} />;
			case "project": return <IoLogoOctocat className={"nav-button-icon"} size={"2rem"} />;
			case "work": return <IoMdBriefcase className={"nav-button-icon"} size={"2rem"} />
		}
	}
}
