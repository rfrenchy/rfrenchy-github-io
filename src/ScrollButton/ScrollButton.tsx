import React from "react";

import { FaAngleUp } from "react-icons/fa";

interface IState {
	isVisible: boolean
}

const VISIBILITY_CHECK_INTERVAL = 300;
const PAGE_OFFSET_THRESHOLD = 150;

export class ScrollButton extends React.Component<any, IState> {

	private visibilityCheckerID: any;

	public constructor(props: any) {
		super(props);

		this.state = { isVisible: false };
		this.updateButtonVisibility = this.updateButtonVisibility.bind(this);
	}

	public render(): JSX.Element {
		let scrollButtonClassName = "scroll-button";

		if (!this.state.isVisible) {
			scrollButtonClassName = scrollButtonClassName.concat(" hidden");
		}

		return (
			<div onClick={this.scrollToTop} className={scrollButtonClassName}>
				<FaAngleUp className="scroll-button-icon" size={"24px"} color="white" />
			</div>)
	}

	public componentDidMount(): void {
		this.visibilityCheckerID = setInterval(this.updateButtonVisibility, VISIBILITY_CHECK_INTERVAL);
	}

	public componentWillUnmount(): void {
		clearInterval(this.visibilityCheckerID);
	}

	private updateButtonVisibility(): void {
		window.pageYOffset > PAGE_OFFSET_THRESHOLD ?
			this.setState({ isVisible: true }) :
			this.setState({ isVisible: false })
	}

	private scrollToTop(): void {
		const body = document.querySelector("body");

		if (body) {
			body.scrollIntoView({ behavior: "smooth", block: "start" })
		}
	}
}

