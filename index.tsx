
import React from "react";
import ReactDOM from "react-dom";

import { Provider } from "react-redux";
import { App } from "./src/App/App";

// Import scss files so they can be bundled by webpack
import "./css/index.scss";

const body = document.querySelector("body");

ReactDOM.render(<App />, body);
