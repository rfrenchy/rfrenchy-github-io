
import React from "react";
import { IoMdSchool } from "react-icons/io";

export class Education extends React.Component {
	public render(): JSX.Element {
		return (
			<div className="education-container">
				<div className="education-wrapper">
					<div className="education-title-wrapper">
						<div className="education-icon"><IoMdSchool size={"6rem"} /></div>
						<div className="education-title">Education</div>
					</div>
					<div className="education-info">
						<div className="education-degree"><span>BSc Computer Science (First Class)</span></div>
						<div className="education-university"><span>Bangor University</span></div>
						<div className="education-dates"><span>2014-2017</span></div>
					</div>

				</div>
			</div>)
	}
}